package com.example.trafficlight;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity {
    private LinearLayout b1, b2, b3;
    private Button btn1;
    private boolean isStarted = false;
    private int counter = 0;
//    Thread thread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b1 = findViewById(R.id.bulb1);
        b2 = findViewById(R.id.bulb2);
        b3 = findViewById(R.id.bulb3);
        btn1 = findViewById(R.id.button1);
    }

    public void onClickStart(View view) {
//        Runnable runnable = () -> {
//
//        };
//        thread = new Thread(runnable);
//        thread.start();

        if (!isStarted) {
            isStarted = true;
            btn1.setText("Stop");
            new Thread(() -> {
                while (isStarted) {
                    counter++;
                    switch (counter) {
                        case 1:
                            b1.setBackgroundColor(getResources().getColor(R.color.red));
                            b2.setBackgroundColor(getResources().getColor(R.color.grey));
                            b3.setBackgroundColor(getResources().getColor(R.color.grey));
                            break;
                        case 2:
                            b1.setBackgroundColor(getResources().getColor(R.color.grey));
                            b2.setBackgroundColor(getResources().getColor(R.color.yellow));
                            b3.setBackgroundColor(getResources().getColor(R.color.grey));
                            break;
                        case 3:
                            b1.setBackgroundColor(getResources().getColor(R.color.grey));
                            b2.setBackgroundColor(getResources().getColor(R.color.grey));
                            b3.setBackgroundColor(getResources().getColor(R.color.green));
                            counter = 0;
                            break;
                    }
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        } else {
            isStarted = false;
            btn1.setText("Start");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        isStarted = false;
        // or kill thread with thread.interrupt()?
    }
}